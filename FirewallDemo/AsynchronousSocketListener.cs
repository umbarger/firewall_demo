﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace FirewallDemo
{
    public class AsynchronousSocketListener
    {
        public ManualResetEvent allDone = new ManualResetEvent(false);

        public int listenPort;

        public AsynchronousSocketListener( int port )
        {
            listenPort = port;
        }

        public void StartListening()
        {
            // Incoming data buffer
            byte[] bytes = new Byte[1024];

            // Assign the local socket information to variables
            string self = Dns.GetHostName();
            IPHostEntry ipHostInfo = Dns.GetHostEntry( self );
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint( ipAddress, listenPort );

            // Make TCP/IP socket
            Socket listener = new Socket( AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp );

            // Bind and start listening
            try
            {
                listener.Bind( localEndPoint );
                listener.Listen( 100 );

                while( true )
                {
                    // Reset event
                    allDone.Reset();

                    // Listen for connections
                    Console.WriteLine( "Listening for connections on LAN..." );
                    listener.BeginAccept( new AsyncCallback( AcceptCallback ), listener );

                    // Don't continue until a connection is made
                    allDone.WaitOne();

                }
            } catch ( Exception e ) { Console.WriteLine( e.ToString() ); }

            Console.WriteLine("\nPress ENTER to continue..." );
            Console.Read();
        }

        public void AcceptCallback( IAsyncResult ar )
        {
            // Signal the main thread to continue
            allDone.Set();

            // Get the socket
            Socket listener = (Socket) ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            // Create the state object
            StateObject state = new StateObject();
            state.workSocket = handler;
            handler.BeginReceive( state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback( ReadCallback ), state );
        }
            
        public void ReadCallback( IAsyncResult ar )
        {
            String content = String.Empty;

            StateObject state = (StateObject) ar.AsyncState;
            Socket handler = state.workSocket;

            // Read data from client's socket
            int bytesRead = handler.EndReceive(ar);

            if ( bytesRead > 0 )
            {
                // Store data in case more data coming
                state.sb.Append( Encoding.ASCII.GetString( state.buffer, 0, bytesRead ) );

                // If EOF tag is not there, read more data
                if ( content.IndexOf( "<EOF>" ) > -1 )
                {
                    // All data read. Display on console.
                    Console.WriteLine( "Read {0} bytes from socket. \n Data :{1}", content.Length, content );
                    Send(handler, content);
                }
                else
                {
                    // Not finished receiving data. Get more.
                    handler.BeginReceive( state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback( ReadCallback ), state );
                }
            }
        }

            private void Send(Socket handler, String data) 
            {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0,
            new AsyncCallback(SendCallback), handler);
        }

        private void SendCallback(IAsyncResult ar) 
        {
            try 
            {
                // Retrieve the socket from the state object.
                Socket handler = (Socket) ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = handler.EndSend(ar);
                Console.WriteLine("Sent {0} bytes to client.", bytesSent);

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();

            } catch (Exception e) {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
