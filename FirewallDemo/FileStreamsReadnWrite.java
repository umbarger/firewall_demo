import java.io.*;

class FileStreamsReadnWrite {
       public static void main(String[] args) {
              try {
                     File InputFile = new File("In.txt");
                     File OutputFile = new File("Out.txt");

                     /*
                      * Constructor of FileInputStream throws FileNotFoundException if
                      * the argument File does not exist.
                      */

                     FileInputStream fis = new FileInputStream(InputFile);
                     FileOutputStream fos = new FileOutputStream(OutputFile);
                     int count;

                     while ((count = fis.read()) != -1) {
                           fos.write(count);
                     }
                     fis.close();
                     fos.close();
              } catch (FileNotFoundException e) {
                     System.err.println("FileStreamsReadnWrite: " + e);
              } catch (IOException e) {
                     System.err.println("FileStreamsReadnWrite: " + e);
              }
       }
}

